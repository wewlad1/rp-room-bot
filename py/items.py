# Copyright 2019 Mystery Coder X <anonbaka9@hotmail.com>
#
# Licensed under the Microsoft Public License (Ms-PL).

import discord
import yaml
import py.common as common
import os


class Item:

    def __init__(self, name, id):
        self.name = name
        self.id = id
        self.desc = ""
        self.closer_desc = ""
        self.container = False
        self.closed = False
        self.inventory = []
        self.portable = True
        self.truth_bullet = False
        self.location = ""

    def display_name(self):
        return self.name.replace("_", " ").capitalize()


class ItemHandler:

    def __init__(self, client):
        self.item_list = {}
        self.load_items_data()
        self.client = client
        self.item_number = len(self.item_list)

    def new_item(self, name, desc):
        self.item_number = len(self.item_list) + 1
        new_item = Item(name, self.item_number)
        new_item.desc = desc
        self.item_list[new_item.id] = new_item
        self.save_items_data()
        return self.item_list[new_item.id]

    def check_object(self, item):
        if not isinstance(item, Item):
            return item
        new_item = Item(item.name, item.id)
        for attribute in new_item.__dict__:
            if not hasattr(item, attribute):
                return self.update_object(item, new_item)

    def update_object(self, item, new_item):
        for attribute in new_item.__dict__:
            try:
                old_attr = getattr(item, attribute)
                setattr(new_item, attribute, old_attr)
            except AttributeError:
                continue
        return new_item

    def fix_1numbers(self):
        number = 1
        for item in sorted(list(self.item_list.values()), key=lambda m: m.id, reverse=False):
            if item.id == number:
                number += 1
                continue
            else:
                old_number = item.id
                item.id = number
                self.item_list[number] = item
                del self.item_list[old_number]
                self.client.fixnumber_inventory(old_number, number)
                number += 1
        self.save_items_data()

    async def find_item(self, itemname):
        player = discord.utils.find(discord.utils.find(lambda m: self.item_list[m].name.lower().
                                              startswith(itemname.lower()), self.item_list))
        if not player:
            player = discord.utils.find(discord.utils.find(lambda m: self.item_list[m].display_name().lower().
                                              startswith(itemname.lower()), self.item_list))
        if not player:
            return None
        else:
            return player

    def find_item_in_list(self, itemname, itemlist):
        item = discord.utils.find(lambda m: m.name.lower().
                                              startswith(itemname.lower()), itemlist)
        if not item:
            item = discord.utils.find(lambda m: m.display_name().lower().
                                              startswith(itemname.lower()), itemlist)
        if not item:
            return None
        else:
            return item

    def convert_list(self, item_list):
        new_list = []
        for item in item_list:
            new_list.append(self.item_list[item])
        return new_list

    def pick_up(self, item_id, player_id):
        player_obj = self.client.players.player_list[player_id]
        item_obj = self.item_list[item_id]
        if not item_obj.location:
            return
        location_obj = self.client.get_item_location(item_obj.location)
        player_obj.inventory.append(item_obj.id)
        location_obj.inventory.remove(item_obj.id)
        item_obj.location = player_obj.id

    def return_truthbullets(self):
        new_list = []
        for item in self.item_list.values():
            if item.truth_bullet:
                new_list.append(item)
        return new_list

    def load_items_data(self):
        try:
            with open(common.YAML_ITEM_FILENAME, 'r') as stream:
                try:
                    self.item_list = yaml.unsafe_load(stream)
                    if not self.item_list:
                        self.item_list = {}
                except yaml.YAMLError as exc:
                    print(exc)
        except FileNotFoundError:
            self.save_items_data()

    def save_items_data(self):
        try:
            os.mkdir(common.YAML_GAMEDATA_FOLDER)
        except FileExistsError:
            pass
        with open(common.YAML_ITEM_FILENAME, 'w') as outfile:
            yaml.dump(self.item_list, outfile, default_flow_style=False)