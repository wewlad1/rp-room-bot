# Copyright 2019 Mystery Coder X <anonbaka9@hotmail.com>
#
# Licensed under the Microsoft Public License (Ms-PL).

import discord
import os
import yaml
import py.common as common


class Room:

    def __init__(self):
        self.name = None
        self.shortcut = None
        self.location_name = None
        self.channel_id = None
        self.desc = None
        self.connection = []
        self.inventory = []
        self.locked = False
        self.lockable = True
        self.category = None
        self.maplock = False

    def get_object(self, client):
        return client.rooms.get_room_obj(id=self.channel_id)

    def display_name(self):
        return self.name.replace("_", " ").capitalize()

    def remove_player(self, player_obj, client):
        RoomsHandler.remove_player_from_room(player_obj, self.get_object(client), client)

    def add_connection(self, new_connection):
        self.connection.append(new_connection)

    def remove_connection(self, rem_connection):
        self.connection.remove(rem_connection)

    async def create_channel(self, client):
        perms_dict = {}
        perms_player = discord.PermissionOverwrite()
        perms_spec = discord.PermissionOverwrite()
        perms_everyone = discord.PermissionOverwrite()
        perms_player.read_messages = False
        perms_player.read_message_history = False
        perms_everyone.read_messages = False
        perms_spec.send_messages = False
        perms_dict[client.player_role] = perms_player
        perms_dict[client.spec_role] = perms_spec
        perms_dict[client.everyone_role] = perms_everyone
        obj = await client.main_guild.create_text_channel(self.name, overwrites=perms_dict)
        self.channel_id = obj.id
        return obj.id

    async def delete_channel(self, client):
        room_obj = client.main_guild.get_channel(self.channel_id)
        if room_obj:
            await room_obj.delete()

class RoomsHandler:

    def __init__(self, client):
        self.client = client
        self.room_list = {}
        self.load_rooms_data()

    async def make_new_room(self, name):
        n_room = Room()
        n_room.name = name
        id = await n_room.create_channel(self.client)
        self.room_list[n_room.name] = n_room
        self.room_list[n_room.channel_id] = n_room
        self.save_rooms_data()
        return id

    def find_room(self, roomname):
        room = discord.utils.find(lambda m: self.room_list[m].name.lower().
                                              startswith(roomname.lower()), self.client.rooms.room_list)
        if not room:
            room = discord.utils.find(lambda m: self.room_list[m].dislay_name().lower().
                                              startswith(roomname.lower()), self.client.rooms.room_list)
        if not room:
            return None
        else:
            return room

    def check_object(self, room):
        if not isinstance(room, Room):
            return room
        new_room = Room()
        for attribute in new_room.__dict__:
            if not hasattr(room, attribute):
                return self.update_object(room, new_room)

    def update_object(self, room, new_room):
        for attribute in new_room.__dict__:
            try:
                old_attr = getattr(room, attribute)
                setattr(new_room, attribute, old_attr)
            except AttributeError:
                continue
        return new_room

    def refresh_room(self, name):
        room = self.room_list[name]
        room_obj = self.get_room_obj(name=name)
        if room_obj:
            room_obj.name = room.name
            if room.desc:
                room_obj.topic = room.desc
            if room.category_id:
                room_obj.category_id = room.category_id

    def get_room_obj(self, **kwargs):
        name_r = kwargs.get('name', None)
        id_r = kwargs.get('id', None)
        if name_r:
            return self.client.main_guild.get_channel(self.room_list[name_r].channel_id)
        if id_r:
            return self.client.main_guild.get_channel(id_r)
        return None

    async def set_room_desc(self, channel_name, new_desc):
        room_obj = self.room_list[channel_name]
        channel_obj = self.client.main_guild.get_channel(room_obj.channel_id)
        if channel_obj:
            room_obj.desc = new_desc
            await channel_obj.edit(topic=new_desc)

    def add_connections(self, connection1, connection2):
        room1 = self.room_list[connection1]
        room2 = self.room_list[connection2]
        room1.add_connection(room2.name)
        room2.add_connection(room1.name)

    def remove_connections(self, connection1, connection2):
        room1 = self.room_list[connection1]
        room2 = self.room_list[connection2]
        room1.remove_connection(room2.name)
        room2.remove_connection(room1.name)

    def load_rooms_data(self):
        try:
            with open(common.YAML_CHANNELS_FILENAME, 'r') as stream:
                try:
                    self.room_list = yaml.unsafe_load(stream)
                    if not self.room_list:
                        self.room_list = {}
                except yaml.YAMLError as exc:
                    print(exc)
        except FileNotFoundError:
            self.save_rooms_data()

    def save_rooms_data(self):
        try:
            os.mkdir(common.YAML_GAMEDATA_FOLDER)
        except FileExistsError:
            pass
        with open(common.YAML_CHANNELS_FILENAME, 'w') as outfile:
            yaml.dump(self.room_list, outfile, default_flow_style=False)
