# Copyright 2019 Mystery Coder X <anonbaka9@hotmail.com>
#
# Licensed under the Microsoft Public License (Ms-PL).

import discord
import asyncio
import itertools
import traceback
import os
import py.common as common
from py.commands import Commands
import py.player as Players
import py.room as Rooms
import py.searcher as Searcher
import py.items as Items
import py.botsettings as BotSettings
import py.bothandler as Bot
import py.ignorelist as Ignore
import py.musicsetting as MusicSetting
import py.musichandler as MusicBot


class Client(discord.client.Client):

    def __init__(self, programsettings):
        discord.client.Client.__init__(self)
        self.settings = programsettings
        self.commands = Commands(self)
        self.ignore = None
        self.rooms = None
        self.players = None
        self.searcher = None
        self.items = None
        self.main_guild = None
        self.announcement_channel = None
        self.global_pause = False
        self.admin_role = None
        self.player_role = None
        self.spec_role = None
        self.everyone_role = None
        self.bots = None
        self.musicbots = None
        self.loop = asyncio.get_event_loop()
        self.loop.create_task(self.start(self.settings.token))
        self.initialize_bots()

    async def on_ready(self):
        await self.wait_until_ready()
        print(common.NORMAL_WELCOME_MESSAGE)
        self.initialize_items()
        await self.initialize_roles()
        await self.players.initialize_players()

    def initialize_bots(self):
        self.bots = {}
        self.musicbots = {}
        try:
            for filename in os.listdir(common.YAML_BOT_DIR):
                botset = BotSettings.BotSettings(common.YAML_BOT_DIR + filename)
                bot = Bot.PlayerBot(botset, self)
                self.bots[botset.id] = bot
        except WindowsError:
            try:
                os.mkdir(common.YAML_BOT_DIR)
            except FileExistsError:
                pass
        try:

            for filename in os.listdir(common.YAML_MUSICBOT_DIR):
                musicset = MusicSetting.MusicSettings(common.YAML_MUSICBOT_DIR + filename)
                bot = MusicBot.MusicBot(musicset, self)
                self.musicbots[musicset.id] = bot
        except WindowsError:
            try:
                os.mkdir(common.YAML_MUSICBOT_DIR)
            except FileExistsError:
                pass
        self.loop.run_forever()

    def initialize_items(self):
        self.main_guild = self.get_guild(int(self.settings.guild))
        if self.main_guild is None:
            raise AttributeError("Invalid server ID")
        self.announcement_channel = self.main_guild.get_channel(int(self.settings.announcement_channel))
        self.rooms = Rooms.RoomsHandler(self)
        self.players = Players.PlayerHandler(self)
        self.searcher = Searcher.Searcher(self)
        self.items = Items.ItemHandler(self)
        self.ignore = Ignore.IgnoreList()

    async def initialize_roles(self):
        role_list = self.main_guild.roles
        self.everyone_role = role_list[0]
        self.admin_role = discord.utils.find(lambda m: m.name == self.settings.admin_role, role_list)
        self.player_role = discord.utils.find(lambda m: m.name == self.settings.player_role, role_list)
        self.spec_role = discord.utils.find(lambda m: m.name == self.settings.spec_role, role_list)
        if not self.admin_role:
            self.admin_role = await self.main_guild.create_role(name=self.settings.admin_role, colour=common.DISCORD_ADMIN_COLOUR)
        if not self.player_role:
            self.player_role = await self.main_guild.create_role(name=self.settings.player_role, colour=common.DISCORD_PLAYER_COLOUR)
        if not self.spec_role:
            self.spec_role = self.main_guild.create_role(name=self.settings.spec_role, colour=common.DISCORD_SPECTATOR_COLOUR)

    async def on_message(self, message):
        result = 0
        if message.author == self.user:
            return
        if message.channel.id in self.ignore.ignore_list:
            return
        if message.content.startswith(common.YAML_COMMAND_CHAR):
            message_content = message.content.strip(common.YAML_COMMAND_CHAR).split()
            command_name = message_content[0]
            argoments = message_content[1:]
            try:
                result = await self.commands.run_command(message, command_name, argoments)
            except AttributeError as e:
                print("Command " + command_name + " does not exist. " + traceback.format_exc())
                await message.channel.send("Command " + command_name + " does not exist.")
            if result == 2:
                await message.channel.send("You do not have permission to do that.")
        elif message.guild and message.guild == self.main_guild and message.channel.id in self.rooms.room_list:
            player_list = self.get_players_in_loc(message.channel.id)
            for player in player_list:
                player_obj = self.players.player_list[player]
                if player_obj.notepad_setting >= 2 or player_obj.hidden:
                    ntpd = self.main_guild.get_channel(player_obj.notepad)
                    if not ntpd:
                        return
                    name = message.author.nick
                    if name is None:
                        name = message.author.name
                    await ntpd.send(name + ": " + message.content)

    async def move_room(self, player_id, from_location_id, to_location_id):
        from_obj = None
        if from_location_id:
            from_obj = self.rooms.room_list[from_location_id]
        to_obj = self.rooms.room_list[to_location_id]
        ply_obj = self.players.player_list[player_id]
        ply_obj.location = to_location_id
        await self.transfer_room(player_id, from_location_id, to_location_id)
        try:
            await self.main_guild.get_channel(from_location_id).send(ply_obj.name + " has left to " + to_obj.name)
            await self.main_guild.get_channel(to_location_id).send(ply_obj.name + " has arrived from " + from_obj.name)
            await self.print_player_status(player_id, to_location_id)
            msg = await self.print_room_desc(ply_obj.id, True)
            if not ply_obj.bot:
                if ply_obj.notepad_setting == 0 or ply_obj.notepad_setting == 2:
                    await self.players.get_player_obj(id=ply_obj.id).send(msg)
                else:
                    channel_obj = self.main_guild.get_channel(ply_obj.notepad)
                    await channel_obj.send(msg)
        except AttributeError:
            await self.main_guild.get_channel(to_location_id).send(ply_obj.name + " has arrived from the void")
            await self.print_player_status(player_id, to_location_id)

    async def print_player_status(self, player_id, location_id):
        ply_obj = self.players.player_list[player_id]
        if ply_obj.desc is None:
            return
        await self.main_guild.get_channel(location_id).send("Status: " + ply_obj.desc)

    async def print_room_desc(self, player_id, print_exits):
        player_obj = self.players.player_list[player_id]
        location_obj = self.rooms.room_list[player_obj.location]
        description = location_obj.desc
        name = location_obj.display_name()
        exits = ""
        items = ""
        players = ""
        for item in location_obj.inventory:
            item_obj = self.items.item_list[item]
            aan = self.return_a_an(item_obj.display_name())
            items = items + "\n There is " + aan + " " + item_obj.display_name() + ". " + item_obj.desc
        if description is None:
            description = ""
        if name is None:
            name = "the void"
        if print_exits:
            exits = "Exits: " + ", ".join(location_obj.connection)
        for player in self.get_players_in_loc(location_obj.channel_id):
            player_obj = self.players.player_list[player]
            if player_obj.id == player_id:
                continue
            players = "\n" + player_obj.name + " is here: " + player_obj.desc
        final_string = "**" + name + "** \n > " + description + "\n > " + exits + "\n" + items + "\n" + players
        return final_string

    def return_a_an(self, string):
        if string[0] in "aeiouAEIOU":
            return "an"
        else:
            return "a"

    async def transfer_room(self, player_id, from_location_id, to_location_id):
        if from_location_id:
            await self.remove_room_perms(player_id, from_location_id)
        await self.grant_room_perms(player_id, to_location_id)

    async def grant_room_perms(self, player_id, loc_id):
        loc_obj = self.main_guild.get_channel(loc_id)
        ply_obj = self.main_guild.get_member(player_id)
        perms = discord.PermissionOverwrite()
        perms.read_messages = True
        perms.send_messages = True
        perms.read_message_history = False
        perms.add_reactions = True
        await loc_obj.set_permissions(ply_obj, overwrite=perms)

    async def grant_room_perms_spectator(self, player_id, loc_id):
        loc_obj = self.main_guild.get_channel(loc_id)
        ply_obj = self.main_guild.get_member(player_id)
        perms = discord.PermissionOverwrite()
        perms.read_messages = True
        perms.send_messages = True
        perms.add_reactions = True
        await loc_obj.set_permissions(ply_obj, overwrite=perms)

    async def remove_room_perms(self, player_id, loc_id):
        loc_obj = self.main_guild.get_channel(loc_id)
        ply_obj = self.main_guild.get_member(player_id)
        perms = discord.PermissionOverwrite()
        perms.read_messages = False
        perms.send_messages = False
        perms.read_message_history = False
        perms.add_reactions = False
        await loc_obj.set_permissions(ply_obj, overwrite=perms)

    async def check_if_occupied(self, room_id, player_obj):
        loc_obj = self.rooms.room_list[room_id]
        for player in self.players.player_list:
            if player == player_obj.id:
                continue
            player2_obj = self.players.player_list[player]
            if player_obj.holding == player2_obj.id:
                continue
            if player2_obj.following == player_obj.id:
                continue
            if player2_obj.location == player_obj.location:
                return True
        return False

    async def remove_items(self):
        for player in self.players.player_list:
            player_obj = self.players.player_list[player]
            for item in player_obj.inventory:
                try:
                    item_obj = self.items.item_list[item]
                except KeyError as e:
                    player_obj.inventory.remove(item)
                    continue
                if item_obj.location == player_obj.id:
                    continue
                else:
                    player_obj.inventory.remove(item)
        for location in self.rooms.room_list:
            location_obj = self.rooms.room_list[location]
            for item in location_obj.inventory:
                try:
                    item_obj = self.items.item_list[item]
                except KeyError as e:
                    location_obj.inventory.remove(item)
                    continue
                if item_obj.location == location_obj.channel_id:
                    continue
                else:
                    location_obj.inventory.remove(item)

    async def restore_items(self):
        for item in self.items.item_list:
            item_obj = self.items.item_list[item]
            if not item_obj.location:
                continue
            container_obj = self.get_item_location(item_obj.location)
            if not container_obj:
                print("Item " + str(item_obj.id) + " " + item_obj.name + " has an invalid location data.")
                continue
            if item_obj.id not in container_obj.inventory:
                container_obj.inventory.append(item_obj.id)

    def available_lists(self, player_id, location_id, close_examine):
        loc_obj = self.rooms.room_list[location_id]
        player_obj = self.players.player_list[player_id]
        final_inventory = list(set().union(loc_obj.inventory, player_obj.inventory))
        for item in player_obj.inventory:
            item_obj = self.items.item_list[item]
            if not item_obj.container:
                continue
            final_inventory = list(set().union(final_inventory, item_obj.inventory))
        if not close_examine:
            player_list = self.get_players_in_loc(location_id)
            if player_list:
                for player in player_list:
                    temp_inventory = []
                    play_obj = self.players.player_list[player]
                    if play_obj.id == player_obj.id:
                        continue
                    for item in play_obj.inventory:
                        item_obj = self.items.item_list[item]
                        temp_inventory.append(item)
                    final_inventory = list(set().union(final_inventory, temp_inventory))
        return final_inventory

    def get_players_in_loc(self, location_id):
        loc_obj = self.rooms.room_list[location_id]
        player_list = []
        for players in self.players.player_list:
            player_obj = self.players.player_list[players]
            if player_obj.location == loc_obj.channel_id:
                player_list.append(players)
        return player_list

    def get_item_location(self, location_id):
        container_obj = None
        try:
            container_obj = self.rooms.room_list[location_id]
        except KeyError:
            pass
        try:
            if not container_obj:
                container_obj = self.players.player_list[location_id]
        except KeyError:
            pass
        try:
            if not container_obj:
                container_obj = self.items.item_list[location_id]
        except KeyError:
            pass
        return container_obj

    def pick_list(self, location_id):
        loc_obj = self.rooms.room_list[location_id]
        final_list = []
        for item in loc_obj.inventory:
            item_obj = self.items.item_list[item]
            if item_obj.container and item_obj.inventory:
                for inside_item in item_obj.inventory:
                    item_inside_obj = self.items.item_list[inside_item]
                    if item_inside_obj.portable:
                        final_list.append(inside_item)
            if item_obj.portable:
                final_list.append(item)
        return final_list

    def get_all_inventory(self, player_id):
        player_obj = self.players.player_list[player_id]
        final_list = player_obj.inventory
        for item in player_obj.inventory:
            item_obj = self.items.item_list[item]
            if item_obj.container:
                for inside_item in item_obj.inventory:
                    final_list.append(inside_item.id)
        return final_list

    def get_all_items(self, location_id):
        item_list = filter(lambda m: m.location == location_id, self.items.item_list.values())
        player_list = filter(lambda m: m.location == location_id, self.players.player_list.values())
        item_players = []
        for players in player_list:
            item_players = itertools.chain(item_players, filter(lambda m: m.location == players.id, self.items.item_list.values()))
        final_list = set(itertools.chain(item_players, item_list))
        return final_list

    async def rename_multiple(self, location_id):
        all_list = self.get_all_items(location_id)
        counter = {key: key.name for key in all_list}
        counter = {key: 1 for key in counter.items()}
        final_list = {}
        for item in all_list:
            if item.name in final_list:
                counter[item.name] += 1
                item.name = item.name + " " + counter[item.name]
                item.renamed = True
            final_list[item.name] = item
        self.items.save_items_data()

    async def check_rename(self, location_id):
        all_list = self.get_all_items(location_id)
        for item in all_list:
            if item.renamed:
                item.name = item.name[:-2]
        self.items.save_items_data()

    def same_area_player_check(self, user, target):
        if target.hidden:
            return False
        return user.location == target.location

    async def return_message(self, player, msgobj, message):
        if msgobj.author.bot:
            await self.bots[player.bot_id].channel.send(message)
            return
        if player.notepad_setting == 0 or player.notepad_setting == 2:
            await msgobj.author.send(message)
        else:
            channel_obj = self.main_guild.get_channel(player.notepad)
            await channel_obj.send(message)

    async def hide_player(self, player, location):
        await self.remove_room_perms(player.id, location.channel_id)

    async def unhide_player(self, player, location):
        await self.grant_room_perms(player.id, location.channel_id)

    async def send_announcement(self, message):
        await self.announcement_channel.send(message)

    def check_player_role(self, member_id, role):
        guild_member = self.main_guild.get_member(member_id)
        if not guild_member:
            return False
        if role in guild_member.roles:
            return True
        else:
            return False

    def return_inventory(self, player_id, truth_bullet):
        player_obj = self.players.player_list[player_id]
        item_list = []
        for item in player_obj.inventory:
            item_obj = self.items.item_list[item]
            if truth_bullet == 0 and not item_obj.truth_bullet:
                item_list.append(item_obj)
            if truth_bullet == 1 and item_obj.truth_bullet:
                item_list.append(item_obj)
        return item_list

    def delete_inventory(self, id):
        for player in self.players.player_list:
            player_obj = self.players.player_list[player]
            for item in player_obj.inventory:
                if item != id:
                    continue
                else:
                    player_obj.inventory.remove(item)
                    return
        for location in self.rooms.room_list:
            location_obj = self.rooms.room_list[location]
            for item in location_obj.inventory:
                if item != id:
                    continue
                else:
                    location_obj.inventory.remove(item)
                    return
        for container in self.items.item_list:
            container_obj = self.items.item_list[container]
            if not container_obj.container:
                continue
            for item in container.inventory:
                if item != id:
                    continue
                else:
                    container_obj.inventory.remove(item)
                    return

    def fixnumber_inventory(self, old_id, new_id):
        for player in self.players.player_list:
            player_obj = self.players.player_list[player]
            for item in player_obj.inventory:
                if item != old_id:
                    continue
                else:
                    player_obj.inventory.remove(item)
                    player_obj.inventory.append(new_id)
                    return
        for location in self.rooms.room_list:
            location_obj = self.rooms.room_list[location]
            for item in location_obj.inventory:
                if item != old_id:
                    continue
                else:
                    location_obj.inventory.remove(item)
                    location_obj.inventory.append(new_id)
                    return
        for container in self.items.item_list:
            container_obj = self.items.item_list[container]
            if not container_obj.container:
                continue
            for item in container.inventory:
                if item != old_id:
                    continue
                else:
                    container_obj.inventory.remove(item)
                    container_obj.inventory.append(new_id)
                    return

