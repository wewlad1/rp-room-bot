# Copyright 2019 Mystery Coder X <anonbaka9@hotmail.com>
#
# Licensed under the Microsoft Public License (Ms-PL).

import random
import itertools
import discord
import os
import py.room as Room
import py.player as Player
import py.items as Items
import py.common as common

class Commands:

    def __init__(self, client):
        self.client = client
        self.command_log = []

    async def run_command(self, message, command_name, argoments):
            return await getattr(self, "cmd_" + command_name)(message, argoments)

    async def cmd_hello(self, message, args):
        msg = 'Hello {0.author.mention}'.format(message)
        await message.channel.send(msg)

    async def cmd_roll(self, message, args):
        list_num = args[0].split('d')
        times_rolled = int(list_num[0])
        max_num = int(list_num[1])
        results = []
        try:
            for x in range(times_rolled):
                results.append(random.randint(1, max_num))
            num_list = ', '.join(str(x) for x in results)
            msg = message.author.mention + " rolled " + str(sum(results)) + "\n(" + num_list + ")"
            await message.channel.send(msg)
        except TypeError as e:
            print("Inputed arguments not a number. " + str(e))
            await message.channel.send("Inputed arguments not a number.")
            return 1

    async def cmd_addroom(self, message, args):
        if self.client.check_player_role(message.author.id, self.client.player_role) or self.client.check_player_role(message.author.id, self.client.spec_role):
            return 2
        channel_name = "_".join(args)
        if discord.utils.find(lambda m: self.client.rooms.room_list[m].name == channel_name, self.client.rooms.room_list):
            await message.channel.send("Room name already in use.")
            return 1
        id = await self.client.rooms.make_new_room(channel_name)
        new_channel = self.client.main_guild.get_channel(id)
        await message.channel.send("New channel #"+ new_channel.name+ " created.")

    async def cmd_delroom(self, message, args):
        if self.client.check_player_role(message.author.id, self.client.player_role) or self.client.check_player_role(message.author.id, self.client.spec_role):
            return 2
        channel_name = args[0]
        try:
            room_del = self.client.rooms.room_list[channel_name]
        except KeyError:
            channel_obj = list(filter(lambda m: m.name == channel_name, self.client.main_guild.channels))
            if len(channel_obj) == 1:
                await channel_obj[0].delete()
                return 0
            elif len(channel_obj) > 1:
                await message.channel.send("More than one room with that name.")
                return 1
            else:
                await message.channel.send("Room does not exist")
                return 1
        if room_del:
            await room_del.delete_channel(self.client)
            del self.client.rooms.room_lists[room_del.name]
            del self.client.rooms.room_list[room_del.channel_id]
            self.client.rooms.save_rooms_data()
            try:
                test = self.client.rooms.room_list[room_del.name]
                test = self.client.rooms.room_list[room_del.id]
                await message.channel.send("Error in deletion, data in yaml still exists.")
            except (IndexError, KeyError):
                await message.channel.send("Room deleted.")
            return 0
        await message.channel.send("Room does not exist.")
        return 1

    async def cmd_renameroom(self, message, args):
        if self.client.check_player_role(message.author.id, self.client.player_role) or self.client.check_player_role(message.author.id, self.client.spec_role):
            return 2
        channel_name = args[0]
        new_name = args[1]
        try:
            room_rename = self.client.rooms.room_list[channel_name]
        except (IndexError, KeyError):
            await message.channel.send("Room does not exist.")
            return 1
        if room_rename:
            rename_obj = self.client.rooms.get_room_obj(name=room_rename.name)
            old_name = rename_obj.name
            await rename_obj.edit(name=new_name)
            room_rename.name = new_name
            self.client.rooms.room_list[new_name] = room_rename
            del self.client.rooms.room_list[channel_name]
            self.client.rooms.save_rooms_data()
            await message.channel.send("Old room #"+ old_name + " changed to #" + new_name)
            return 0
        await message.channel.send("Room does not exist.")
        return 1

    async def cmd_fixroomdata(self, message, args):
        if self.client.check_player_role(message.author.id, self.client.player_role) or self.client.check_player_role(message.author.id, self.client.spec_role):
            return 2
        deleted_list = []
        for room in list(self.client.rooms.room_list.values()):
            room_channel = self.client.main_guild.get_channel(room.channel_id)
            if not room_channel:
                try:
                    if room.name not in deleted_list:
                        deleted_list.append(room.name)
                        del self.client.rooms.room_list[room.name]
                        del self.client.rooms.room_list[room.channel_id]
                        await message.channel.send(room.name + " object's channel no longer exists. Deleting data.")
                        continue
                    else:
                        continue
                except KeyError:
                    continue
            room.connection = list(set(room.connection))
            room.inventory = list(set(room.inventory))
            new_room = self.client.rooms.check_object(room)
            if not isinstance(new_room, Room.Room):
                continue
            self.client.rooms.room_list[room.channel_id] = new_room
            self.client.rooms.room_list[room.name] = new_room
        self.client.rooms.save_rooms_data()

    async def cmd_fixplayerdata(self, message, args):
        if self.client.check_player_role(message.author.id, self.client.player_role) or self.client.check_player_role(message.author.id, self.client.spec_role):
            return 2
        for player in list(self.client.players.player_list.values()):
            player_obj = self.client.main_guild.get_member(player.id)
            player.inventory = list(set(player.inventory))
            try:
                location_obj = self.client.rooms.room_list[player.location]
            except KeyError:
                orig_loc = player.location
                for message in player_obj.history(limit=50):
                    last_location = discord.utils.find(lambda m: m.channel_id == message.channel.id, self.client.rooms.room_list)
                    if not last_location:
                        continue
                    else:
                        await message.channel.send("Player " + player.display_name()
                                                   + " location is invalid, relocated to last known location "
                                                   + last_location.display_name())
                        player.location = last_location.id
                        break
                if orig_loc == player.location:
                    await message.channel.send("Player " + player.display_name()
                                               + " location is invalid, last location cannot be found. Please"
                                                 "change manually.")
            player.inventory = list(set(player.inventory))
            try:
                player.desc = " ".join(player.status)
                del player.status
            except AttributeError:
                pass
            new_player = self.client.players.check_object(player)
            if not isinstance(new_player, Player.Player):
                continue
            self.client.players.player_list[player.id] = new_player
        self.client.players.save_player_data()

    async def cmd_roomdesc(self, message, args):
        if self.client.check_player_role(message.author.id, self.client.player_role) or self.client.check_player_role(message.author.id, self.client.spec_role):
            return 2
        channel_name = args[0]
        try:
            room_desc = self.client.rooms.room_list[channel_name]
        except (IndexError, KeyError):
            await message.channel.send("Room does not exist.")
            return 1
        if room_desc:
            await self.client.rooms.set_room_desc(channel_name, " ".join(args[1:]))
            self.client.rooms.save_rooms_data()
            return 0
        await message.channel.send("Room does not exist.")
        return 1

    async def cmd_fix_perms(self, message, args):
        if self.client.check_player_role(message.author.id, self.client.player_role) or self.client.check_player_role(message.author.id, self.client.spec_role):
            return 2
        for player in self.client.players.player_list:
            if isinstance(player, int):
                continue
            ply_obj = self.client.players.player_list[player]
            mem_obj = self.client.main_guild.get_member(ply_obj.id)
            for rooms in self.client.rooms.room_list:
                room_obj = self.client.rooms.room_list[rooms].get_object(self.client)
                if not room_obj:
                    continue
                print(room_obj.name)
                if not room_obj.id == ply_obj.location and mem_obj in room_obj.members:
                    await self.client.remove_room_perms(ply_obj.id, room_obj.id)
                if room_obj.id == ply_obj.location and mem_obj not in room_obj.members:
                    await self.client.grant_room_perms(ply_obj.id, room_obj.id)
        for member in self.client.main_guild.members:
            if self.client.player_role in member.roles:
                continue
            if self.client.spec_role in member.roles or self.client.admin_role in member.roles:
                for rooms in self.client.rooms.room_list:
                    room_obj = self.client.rooms.room_list[rooms].get_object(self.client)
                    if not room_obj:
                        continue
                    await self.client.grant_room_perms_spectator(member.id, room_obj.id)

    async def cmd_addplayer(self, message, args):
        if self.client.check_player_role(message.author.id, self.client.player_role) or self.client.check_player_role(message.author.id, self.client.spec_role):
            return 2
        player_name = " ".join(args)
        player_obj = discord.utils.find(lambda m: m.name().lower() == player_name.lower(), self.client.main_guild.members)
        if not player_obj:
            await message.channel.send("Member " + player_name + " does not exist!")
            return 1
        if self.client.player_role in player_obj.roles:
            discord.utils.find(lambda m: m.name().lower() == player_name.lower(), self.client.main_guild.members)
            if player_obj.id in self.client.players.player_list:
                await message.channel.send("Already a member!")
            else:
                await message.channel.send("Creating player objects.")
                await self.client.players.create_players()
            return 1
        else:
            await player_obj.add_roles(self.client.player_role)
            self.client.players.create_players()
            return 1

    async def cmd_sendlocation(self, message, args):
        if self.client.check_player_role(message.author.id, self.client.player_role) or self.client.check_player_role(message.author.id, self.client.spec_role):
            return 2
        try:
            player_name = args[0]
            location_name = args[1]
            player_obj = discord.utils.find(lambda m: self.client.players.player_list[m].name.lower().startswith(player_name.lower()), self.client.players.player_list)
            player_obj = self.client.players.player_list[player_obj]
            location_obj = discord.utils.find(lambda m: self.client.rooms.room_list[m].name.lower().startswith(location_name.lower()), self.client.rooms.room_list)
            location_obj = self.client.rooms.room_list[location_obj]
            origin_obj = None
        except (IndexError, KeyError) as e:
            await message.channel.send("Argument error. Message: " + e.__class__.__name__ + " : " + str(e))
            return 1
        if not player_obj:
            await message.channel.send("Member " + player_name + " does not exist!")
            return 1
        if player_obj.location:
            origin_obj = discord.utils.find(
                lambda m: self.client.rooms.room_list[m].channel_id == player_obj.location,
                self.client.rooms.room_list)
            if origin_obj:
                origin_obj = self.client.rooms.room_list[origin_obj]
        if not location_obj:
            await message.channel.send("Location " + location_name + " does not exist!")
            return 1
        if player_obj.location == location_obj.channel_id:
            await message.channel.send("Player already in location")
            return 1
        else:
            try:
                await self.client.move_room(player_obj.id,  origin_obj.channel_id, location_obj.channel_id)
            except AttributeError as e:
                await self.client.move_room(player_obj.id,  None, location_obj.channel_id)
            self.client.players.save_player_data()
            return 1

    async def cmd_sendlocationall(self, message, args):
        if self.client.check_player_role(message.author.id, self.client.player_role) or self.client.check_player_role(message.author.id, self.client.spec_role):
            return 2
        try:
            location_name = args[0]
            location_obj = discord.utils.find(lambda m: self.client.rooms.room_list[m].name.lower().startswith(location_name.lower()), self.client.rooms.room_list)
            location_obj = self.client.rooms.room_list[location_obj]
        except (IndexError, KeyError) as e:
            await message.channel.send("Argument error. Message: " + e.__class__.__name__ + " : " + str(e))
            return 1
        if not location_obj:
            await message.channel.send("Location " + location_name + " does not exist!")
            return 1
        for player_obj in self.client.players.player_list.values():
            if not player_obj:
                await message.channel.send("Member " + player_obj.display_name() + " does not exist!")
                continue
            await self.client.move_room(player_obj.id,  None, location_obj.channel_id)
        self.client.players.save_player_data()
        return 1

    async def cmd_createconnection(self, message, args):
        if self.client.check_player_role(message.author.id, self.client.player_role) or self.client.check_player_role(message.author.id, self.client.spec_role):
            return 2
        try:
            conn_name1 = args[0]
            conn_name2 = args[1]
            con_obj1 = self.client.rooms.room_list[conn_name1]
            con_obj2 = self.client.rooms.room_list[conn_name2]
        except (IndexError, KeyError) as e:
            await message.channel.send("Argument error. Message: " + e.__class__.__name__ + " : " + str(e))
            return 1
        self.client.rooms.add_connections(conn_name1, conn_name2)
        self.client.rooms.save_rooms_data()
        await message.channel.send(conn_name1 + " connected to " + conn_name2)

    async def cmd_remove_connection(self, message, args):
        if self.client.check_player_role(message.author.id, self.client.player_role) or self.client.check_player_role(message.author.id, self.client.spec_role):
            return 2
        try:
            conn_name1 = args[0]
            conn_name2 = args[1]
            con_obj1 = self.client.rooms.room_list[conn_name1]
            con_obj2 = self.client.rooms.room_list[conn_name2]
        except (IndexError, KeyError) as e:
            await message.channel.send("Argument error. Message: " + e.__class__.__name__ + " : " + str(e))
            return 1
        self.client.rooms.remove_connections(conn_name1, conn_name2)
        self.client.rooms.save_rooms_data()
        await message.channel.send(conn_name1 + " unlinked " + conn_name2)

    async def cmd_list_path(self, message, args):
        if self.client.check_player_role(message.author.id, self.client.player_role) or self.client.check_player_role(message.author.id, self.client.spec_role):
            return 2
        try:
            conn_name1 = args[0]
            conn_name2 = args[1]
            con_obj1 = self.client.rooms.room_list[conn_name1]
            con_obj2 = self.client.rooms.room_list[conn_name2]
        except (IndexError, KeyError) as e:
            await message.channel.send("Argument error. Message: " + e.__class__.__name__ + " : " + str(e))
            return 1
        path_list = self.client.searcher.search_path(conn_name1, conn_name2)
        if not path_list:
            await message.channel.send("No path cannot be found for " + conn_name1 + " and " + conn_name2)
            return 1
        else:
            await message.channel.send(path_list)

    async def cmd_move(self, message, args):
        if self.client.check_player_role(message.author.id, self.client.admin_role) or self.client.check_player_role(message.author.id, self.client.spec_role):
            return 2
        try:
            move_name = "_".join(args)
            move_obj = discord.utils.find(lambda m: self.client.rooms.room_list[m].name.lower().
                                              startswith(move_name.lower()), self.client.rooms.room_list)
            if not move_obj:
                move_obj = discord.utils.find(lambda m: self.client.rooms.room_list[m].shortcut == move_name.lower(), self.client.rooms.room_list)
            move_obj = self.client.rooms.room_list[move_obj]
            player_obj = self.client.players.player_list[message.author.id]
            player_location = self.client.rooms.room_list[player_obj.location].name
            holding = player_obj.holding != 0
            if holding:
                dragging_obj = self.client.players.player_list[player_obj.holding]
            followers = self.client.players.get_player_followers(player_obj.id)
        except (IndexError, KeyError) as e:
            await message.channel.send("Argument error. Message: " + e.__class__.__name__ + " : " + str(e))
            return 1
        if not player_obj.movement or self.client.global_pause:
            await self.client.return_message(player_obj, message, "You cannot move for one reason or another.")
            return 1
        check_hold = self.client.players.check_holding(player_obj.id)
        if check_hold:
            await self.client.return_message(player_obj, message, "You are being held by " + check_hold + "!")
            return 1
        if player_obj.following != 0:
            await self.client.return_message(player_obj, message, "You cannot move on your own while following someone.")
            return 1
        path_list = self.client.searcher.search_path(player_location, move_obj.name)
        if not path_list:
            await message.channel.send("Cannot move to " + move_obj.name + " from here.")
            return 1
        else:
            for next_name in path_list:
                next_obj = self.client.rooms.room_list[next_name]
                from_obj = self.client.rooms.room_list[player_obj.location]
                if next_obj.name == from_obj.name:
                    continue
                if next_obj.locked:
                    await self.client.return_message(player_obj, message, "Room " + next_obj.name + " locked.")
                    return 1
                if from_obj.locked:
                    await self.client.return_message(player_obj, message, "Room " + from_obj.name + " locked.")
                    return 1
                try:
                    await self.client.move_room(player_obj.id, from_obj.channel_id, next_obj.channel_id)
                    if holding:
                        await self.client.move_room(dragging_obj.id, from_obj.channel_id, next_obj.channel_id)
                    for follower in followers:
                        await self.client.move_room(follower.id, from_obj.channel_id, next_obj.channel_id)
                except AttributeError as e:
                    await self.client.move_room(player_obj.id, None, next_obj.channel_id)
                    if holding:
                        await self.client.move_room(dragging_obj.id, None, next_obj.channel_id)
                    for follower in followers:
                        await self.client.move_room(follower.id, from_obj.channel_id, next_obj.channel_id)
                self.client.players.save_player_data()
                if await self.client.check_if_occupied(next_obj.channel_id, player_obj):
                    await self.client.return_message(player_obj, message, "Stopped in room " + next_obj.name
                                                     + ". Person is here.")
                    return 1

    async def cmd_look(self, message, args):
        if self.client.check_player_role(message.author.id, self.client.admin_role) or self.client.check_player_role(message.author.id, self.client.spec_role):
            return 2
        room_desc = await self.client.print_room_desc(message.author.id, True)
        player_obj = self.client.players.player_list[message.author.id]
        await self.client.return_message(player_obj, message, room_desc)

    async def cmd_lock(self, message, args):
        if self.client.check_player_role(message.author.id, self.client.admin_role) or self.client.check_player_role(message.author.id, self.client.spec_role):
            return 2
        player_obj = self.client.players.player_list[message.author.id]
        location_obj = self.client.rooms.room_list[player_obj.location]
        location_channel = self.client.main_guild.get_channel(location_obj.channel_id)
        if location_obj.lockable:
            location_obj.locked = not location_obj.locked
            if location_obj.locked:
                await location_channel.send("Room locked.")
            else:
                await location_channel.send("Room unlocked.")
        else:
            message.channel.send("Room cannot be locked.")

    async def cmd_gmlock(self, message, args):
        if self.client.check_player_role(message.author.id, self.client.player_role) or self.client.check_player_role(message.author.id, self.client.spec_role):
            return 2
        try:
            location_name = args[0]
            location_obj2 = discord.utils.find(lambda m: self.client.rooms.room_list[m].name.lower().
                                              startswith(location_name.lower()), self.client.rooms.room_list)
            location_obj = self.client.rooms.room_list[location_obj2]
        except (IndexError, KeyError) as e:
            await message.channel.send("Argument error. Message: " + e.__class__.__name__ + " : " + str(e))
            return 1
        if location_obj.lockable:
            location_channel = self.client.main_guild.get_channel(location_obj.channel_id)
            location_obj.locked = not location_obj.locked
            if location_obj.locked:
                await message.channel.send("Room locked.")
                await location_channel.send("Suddenly, the room locked itself.")
            else:
                await message.channel.send("Room unlocked.")
                await location_channel.send("Suddenly, the room unlocked itself.")
            self.client.rooms.save_rooms_data()
        else:
            await message.channel.send("Room cannot be locked.")

    async def cmd_additem(self, message, args):
        if self.client.admin_role not in message.author.roles:
            return 2
        try:
            items = [list(group) for k, group in itertools.groupby(args, lambda x: x == "|") if not k]
            item_name = " ".join(items[0])
            item_desc = " ".join(items[1])
        except (IndexError, KeyError) as e:
            await message.channel.send("Argument error. Message: " + e.__class__.__name__ + " : " + str(e))
            return 1
        new_item = self.client.items.new_item(item_name, item_desc)
        if new_item:
            await message.channel.send("Created item No: " + str(new_item.id) + " , " + new_item.name)
            return 1

    async def cmd_unassigneditems(self, message, args):
        if self.client.admin_role not in message.author.roles:
            return 2
        item_list = []
        item_list.append("List of unassigned items: \n")
        for item in self.client.items.item_list:
            item_obj = self.client.items.item_list[item]
            if not item_obj.location or item_obj.location == '':
                msg = item_obj.display_name() + ": " + str(item_obj.id) + "\n"
                item_list.append( )
        await message.channel.send("".join(item_list))

    async def cmd_putitemlocation(self, message, args):
        if self.client.admin_role not in message.author.roles:
            return 2
        try:
            item_obj = self.client.items.item_list[int(args[0])]
            location_name = discord.utils.find(
            lambda m: self.client.rooms.room_list[m].name.lower().startswith(args[1].lower()),
            self.client.rooms.room_list)
            location_obj = self.client.rooms.room_list[location_name]
        except (IndexError, KeyError, ValueError) as e:
            await message.channel.send("Argument error. Message: " + e.__class__.__name__ + " : " + str(e))
            return 1
        item_obj.location = location_obj.channel_id
        location_obj.inventory.append(item_obj.id)
        await message.channel.send("Item " + item_obj.display_name() + " has been put inside room " + location_obj.display_name())
        self.client.rooms.save_rooms_data()
        self.client.items.save_items_data()

    async def cmd_putitemplayer(self, message, args):
        if self.client.admin_role not in message.author.roles:
            return 2
        try:
            item_obj = self.client.items.item_list[int(args[0])]
            player_name = discord.utils.find(
                lambda m: self.client.players.player_list[m].name.lower().startswith(args[1].lower()),
                self.client.players.player_list)
            player_obj = self.client.players.player_list[player_name]
        except (IndexError, KeyError, ValueError) as e:
            await message.channel.send("Argument error. Message: " + e.__class__.__name__ + " : " + str(e))
            return 1
        item_obj.location = player_obj.id
        player_obj.inventory.append(item_obj.id)
        await message.channel.send("Item " + item_obj.display_name() + " has been given to " + player_obj.display_name())
        self.client.players.save_player_data()
        self.client.items.save_items_data()

    async def cmd_putitem_item(self, message, args):
        if self.client.admin_role not in message.author.roles:
            return 2
        try:
            item_obj = self.client.items.item_list[int(args[0])]
            container_obj = self.client.items.item_list[args[1]]
        except (IndexError, KeyError, ValueError) as e:
            await message.channel.send("Argument error. Message: " + e.__class__.__name__ + " : " + str(e))
            return 1
        item_obj.location = container_obj.id
        container_obj.inventory.append(item_obj.id)
        await message.channel.send("Item " + item_obj.display_name() + " has been put inside" + container_obj.display_name())
        self.client.rooms.save_rooms_data()
        self.client.items.save_players_data()

    async def cmd_fixitems(self, message, args):
        if self.client.admin_role not in message.author.roles:
            return 2
        await self.client.remove_items()
        await self.client.restore_items()
        for item in self.client.items.item_list.values():
            new_item = self.client.items.check_object(item)
            if not isinstance(new_item, Items.Item):
                continue
            self.client.items.item_list[item.id] = new_item
        self.client.items.save_items_data()
        self.client.players.save_player_data()
        self.client.rooms.save_rooms_data()

    async def cmd_examine(self, message, args):
        if self.client.check_player_role(message.author.id, self.client.admin_role) or self.client.check_player_role(message.author.id, self.client.spec_role):
            return 2
        try:
            item_name = args[0]
            player_obj = self.client.players.player_list[message.author.id]
            location_obj = self.client.rooms.room_list[player_obj.location]
            channel_obj = self.client.main_guild.get_channel(location_obj.channel_id)
        except (IndexError, KeyError) as e:
            await message.channel.send("Argument error. Message: " + e.__class__.__name__ + " : " + str(e))
            return 1
        item_list = self.client.available_lists(player_obj.id, location_obj.channel_id, False)
        item_list = self.client.items.convert_list(item_list)
        item_obj = self.client.items.find_item_in_list(item_name, item_list)
        if item_obj:
            await self.client.return_message(player_obj, message, "Examining " + item_name + ": " +
                                             item_obj.desc + "\n" + item_obj.closer_desc )
            return 1
        else:
            await self.client.return_message(player_obj, message, "Cannot find said object.")

    async def cmd_get(self, message, args):
        if self.client.check_player_role(message.author.id, self.client.admin_role) or self.client.check_player_role(message.author.id, self.client.spec_role):
            return 2
        try:
            item_name = " ".join(args)
            player_obj = self.client.players.player_list[message.author.id]
            location_obj = self.client.rooms.room_list[player_obj.location]
            pickable_objects = self.client.pick_list(location_obj.channel_id)
        except (IndexError, KeyError) as e:
            await message.channel.send("Argument error. Message: " + e.__class__.__name__ + " : " + str(e))
            return 1
        item_list = self.client.items.convert_list(self.client.items.item_list)
        item_obj = self.client.items.find_item_in_list(item_name, item_list)
        if not item_obj:
            await self.client.return_message(player_obj, message, "You cannot get that item. \n"
                                "Either it's in the inventory of another, or it doesn't exist.")
            return 1
        if not item_obj.portable:
            await self.client.return_message(player_obj, message, "That is hardly portable.")
            return 1
        if item_obj not in self.client.items.convert_list(pickable_objects):
            await self.client.return_message(player_obj, message, "You cannot get that item. \n"
                                "Either it's in the inventory of another, or it doesn't exist.")
            return 1
        if item_obj:
            self.client.items.pick_up(item_obj.id, player_obj.id)
            channel_obj = self.client.main_guild.get_channel(location_obj.channel_id)
            await channel_obj.send(player_obj.display_name() + " picks up " + item_obj.display_name())
            self.client.rooms.save_rooms_data()
            self.client.players.save_player_data()
            self.client.items.save_items_data()
            return 1

    async def cmd_drop(self, message, args):
        if self.client.check_player_role(message.author.id, self.client.admin_role) or self.client.check_player_role(message.author.id, self.client.spec_role):
            return 2
        try:
            item_name = " ".join(args)
            player_obj = self.client.players.player_list[message.author.id]
            location_obj = self.client.rooms.room_list[player_obj.location]
        except (IndexError, KeyError) as e:
            await message.channel.send("Argument error. Message: " + e.__class__.__name__ + " : " + str(e))
            return 1
        item_list = self.client.items.convert_list(self.client.items.item_list)
        item_obj = self.client.items.find_item_in_list(item_name, item_list)
        if item_obj.truth_bullet:
            await self.client.return_message(player_obj, message, "That cannot be dropped.")
            return 1
        if item_obj.id not in player_obj.inventory:
            await self.client.return_message(player_obj, message, "You do not have that item.")
            return 1
        player_obj.inventory.remove(item_obj.id)
        location_obj.inventory.append(item_obj.id)
        item_obj.location = location_obj.channel_id
        channel_obj = self.client.main_guild.get_channel(location_obj.channel_id)
        msg = player_obj.display_name() + " drops " + item_obj.display_name() + "."
        await channel_obj.send(msg)
        self.client.items.save_items_data()
        self.client.players.save_player_data()
        self.client.rooms.save_rooms_data()
        return 1

    async def cmd_show(self, message, args):
        if self.client.check_player_role(message.author.id, self.client.admin_role) or self.client.check_player_role(message.author.id, self.client.spec_role):
            return 2
        try:
            item_name = args[0]
            player_obj = self.client.players.player_list[message.author.id]
            location_obj = self.client.rooms.room_list[player_obj.location]
            channel_obj = self.client.main_guild.get_channel(location_obj.channel_id)
        except (IndexError, KeyError) as e:
            await message.channel.send("Argument error. Message: " + e.__class__.__name__ + " : " + str(e))
            return 1
        item_list = self.client.get_all_inventory(player_obj.id)
        item_list = self.client.items.convert_list(item_list)
        item_obj = self.client.items.find_item_in_list(item_name, item_list)
        if item_obj:
            await channel_obj.send(player_obj.display_name() + " presents " + item_obj.display_name() + ": " +
                                   item_obj.desc + "\n" + item_obj.closer_desc)
            return 1
        else:
            await self.client.return_message(player_obj, message, "Cannot find said object.")

    async def cmd_setnotepad(self, message, args):
        if self.client.check_player_role(message.author.id, self.client.admin_role) or self.client.check_player_role(message.author.id, self.client.spec_role):
            return 2
        try:
            status = int(args[0])
            player_obj = self.client.players.player_list[message.author.id]
        except (IndexError, KeyError, ValueError) as e:
            await message.channel.send("Argument error. Message: " + e.__class__.__name__ + " : " + str(e))
            return 1
        if status >= 4 or status < 0:
            await self.client.return_message(player_obj, message, "Error. Set notepad command can only take 0, 1 and 2")
            return
        player_obj.notepad_setting = status
        await self.client.return_message(player_obj, message, "Notepad Status set to " + str(status))
        self.client.players.save_player_data()

    async def cmd_lookgm(self, message, args):
        if self.client.admin_role not in message.author.roles:
            return 2
        location_name = args[0]
        location_obj = discord.utils.find(
            lambda m: self.client.rooms.room_list[m].name.lower().startswith(location_name.lower()),
            self.client.rooms.room_list)
        item_list = []
        item_list.append("List of items in " + location_obj.display_name() + ": \n")
        for item in location_obj.inventory:
            item_obj = self.client.items.item_list[item]
            item_list.append(str(item_obj.id) + ": " + item_obj.display_name() + "\n")
        await message.channel.send("".join(item_list))

    async def cmd_lookitem_player(self, message, args):
        if self.client.admin_role not in message.author.roles:
            return 2
        player_name = args[0]
        player_obj = discord.utils.find(lambda m: self.client.players.player_list[m].name.lower().startswith(player_name.lower()), self.client.players.player_list)
        player_obj = self.client.players.player_list[player_obj]
        item_list = []
        item_list.append("List of items on " + player_obj.display_name() + ": \n")
        for item in player_obj.inventory:
            item_obj = self.client.items.item_list[item]
            item_list.append(str(item_obj.id) + ": " + item_obj.display_name() + "\n")
        await message.channel.send("".join(item_list))

    async def cmd_lookitem_item(self, message, args):
        if self.client.admin_role not in message.author.roles:
            return 2
        try:
            item_id= int(args[0])
            item_obj = self.client.players.player_list[item_id]
        except (KeyError, ValueError):
            await message.channel.send("Error! Invalid item. Must be item id.")
        item_list = []
        item_list.append("List of items on " + item_obj.display_name() + ": \n")
        for item in item_obj.inventory:
            item_obj = self.client.items.item_list[item]
            item_list.append(str(item_obj.id) + ": " + item_obj.display_name() + "\n")
        await message.channel.send("".join(item_list))

    async def cmd_status(self, message, args):
        if not self.client.check_player_role(message.author.id, self.client.player_role):
            return 2
        player_obj = self.client.players.player_list[message.author.id]
        player_obj.desc = " ".join(args)
        await self.client.return_message(player_obj, message, "New status set: " + " ".join(args))
        self.client.players.save_player_data()

    async def cmd_check_status(self, message, args):
        if not self.client.check_player_role(message.author.id, self.client.player_role):
            return 2
        target_name = args[0]
        target_obj = discord.utils.find(lambda m: self.client.players.player_list[m].name.lower().startswith(target_name.lower()), self.client.players.player_list)
        target_obj = self.client.players.player_list[target_obj]
        player_obj = self.client.players.player_list[message.author.id]
        if not target_obj:
            await self.client.return_message(player_obj, message, "Target does not exist!")
        if self.client.same_area_player_check(player_obj, target_obj):
            await self.client.return_message(player_obj, message, target_obj.desc)
        else:
            await self.client.return_message(player_obj, message, "That person is not here.")
        self.client.players.save_player_data()

    async def cmd_grab(self, message, args):
        if not self.client.check_player_role(message.author.id, self.client.player_role):
            return 2
        try:
            target_name = args[0]
            target_obj = discord.utils.find(lambda m: self.client.players.player_list[m].name.lower().startswith(target_name.lower()), self.client.players.player_list)
            target_obj = self.client.players.player_list[target_obj]
            player_obj = self.client.players.player_list[message.author.id]
            location_obj = self.client.rooms.room_list[player_obj.location]
        except (IndexError, KeyError) as e:
            await message.channel.send("Argument error. Message: " + e.__class__.__name__ + " : " + str(e))
            return 1
        if player_obj.holding != 0:
            await self.client.return_message(player_obj, message, "You are already grabbing someone.")
            return 1
        if not target_obj:
            await self.client.return_message(player_obj, message, "Target does not exist!")
            return 1
        if target_obj.id == player_obj.id:
            await self.client.return_message(player_obj, message, "You can't grab yourself.")
            return 1
        if not player_obj.movement:
            await self.client.return_message(player_obj, message, "You are too restricted to grab someone!")
            return 1
        if self.client.same_area_player_check(player_obj, target_obj):
            loc_channel = self.client.main_guild.get_channel(location_obj.channel_id)
            await loc_channel.send(player_obj.display_name() + " grabs " + target_obj.display_name() + "!")
            player_obj.holding = target_obj.id
        self.client.players.save_player_data()

    async def cmd_letgo(self, message, args):
        if not self.client.check_player_role(message.author.id, self.client.player_role):
            return 2
        player_obj = self.client.players.player_list[message.author.id]
        target_obj = self.client.players.player_list[player_obj.holding]
        location_obj = self.client.rooms.room_list[player_obj.location]
        if self.client.same_area_player_check(player_obj, target_obj):
            loc_channel = self.client.main_guild.get_channel(location_obj.channel_id)
            await loc_channel.send(player_obj.display_name() + " lets go of " + target_obj.display_name() + "!")
            player_obj.holding = 0
        self.client.players.save_player_data()

    async def cmd_hide(self, message, args):
        if not self.client.check_player_role(message.author.id, self.client.player_role):
            return 2
        player_obj = self.client.players.player_list[message.author.id]
        location_obj = self.client.rooms.room_list[player_obj.location]
        loc_channel = self.client.main_guild.get_channel(location_obj.channel_id)
        await self.client.hide_player(player_obj, location_obj)
        await loc_channel.send(player_obj.display_name() + " hides!")
        player_obj.hidden = True
        if player_obj.holding != 0:
            hiding_obj = self.client.players.player_list[player_obj.holding]
            await self.client.hide_player(hiding_obj, location_obj)
            await loc_channel.send(hiding_obj.display_name() + " hides!")
            hiding_obj.hidden = True
        self.client.players.save_player_data()

    async def cmd_unhide(self, message, args):
        if not self.client.check_player_role(message.author.id, self.client.player_role):
            return 2
        player_obj = self.client.players.player_list[message.author.id]
        location_obj = self.client.rooms.room_list[player_obj.location]
        loc_channel = self.client.main_guild.get_channel(location_obj.channel_id)
        await self.client.unhide_player(player_obj, location_obj)
        await loc_channel.send(player_obj.display_name() + " appears!")
        player_obj.hidden = False
        if player_obj.holding != 0:
            hiding_obj = self.client.players.player_list[player_obj.holding]
            await self.client.unhide_player(hiding_obj, location_obj)
            await loc_channel.send(hiding_obj.display_name() + " appears!")
            hiding_obj.hidden = False
        self.client.players.save_player_data()

    async def cmd_edit_player(self, message, args):
        if self.client.admin_role not in message.author.roles:
            return 2
        try:
            player_name = args[0]
            attribute = args[1]
            new_val = args[2:]
            player_obj = discord.utils.find(lambda m: self.client.players.player_list[m].name.lower().startswith(player_name.lower()), self.client.players.player_list)
            player_obj = self.client.players.player_list[player_obj]
            original_val = getattr(player_obj, attribute)
            if isinstance(original_val, str):
                new_val = " ".join(new_val)
            elif isinstance(original_val, bool):
                new_val = new_val.lower() == "true"
            elif isinstance(original_val, int):
                new_val = int(new_val)
            elif isinstance(original_val, (list, dict, tuple)):
                message.channel.send("You cannot directly change this.")
                return 1
        except (KeyError, AttributeError, IndexError, ValueError)as e:
            await message.channel.send("Argument error. Message: " + e.__class__.__name__ + " : " + str(e))
            return 1
        try:
            setattr(player_obj, attribute, new_val)
            await message.channel.send(attribute + " of " + player_obj.name + " with the old value of " + str(original_val) + " is changed by " + str(new_val))
        except (KeyError, AttributeError)as e:
            await message.channel.send("Argument error. Message: " + e.__class__.__name__ + " : " + str(e))
            return 1
        self.client.players.save_player_data()

    async def cmd_edit_room(self, message, args):
        if self.client.admin_role not in message.author.roles:
            return 2
        try:
            location_name = args[0]
            attribute = args[1]
            new_val = args[2:]
            location_obj = discord.utils.find(
                lambda m: self.client.rooms.room_list[m].name.lower().startswith(location_name.lower()),
                self.client.rooms.room_list)
            location_obj = self.client.rooms.room_list[location_obj]
            original_val = getattr(location_obj, attribute)
            if isinstance(original_val, str):
                new_val = " ".join(new_val)
            elif isinstance(original_val, bool):
                new_val = " ".join(new_val).lower() == "true"
            elif isinstance(original_val, int):
                new_val = int(new_val)
            elif isinstance(original_val, (list, dict, tuple)):
                await message.channel.send("You cannot directly change this.")
                return 1
        except (KeyError, AttributeError, IndexError, ValueError)as e:
            await message.channel.send("Argument error. Message: " + e.__class__.__name__ + " : " + str(e))
            return 1
        try:
            setattr(location_obj, attribute, new_val)
            await message.channel.send(attribute + " of " + location_obj.display_name() + " with the old value of " + str(original_val) + " is changed by " + str(new_val))
        except (KeyError, AttributeError)as e:
            await message.channel.send("Argument error. Message: " + e.__class__.__name__ + " : " + str(e))
            return 1
        self.client.rooms.save_rooms_data()

    async def cmd_edit_item(self, message, args):
        if self.client.admin_role not in message.author.roles:
            return 2
        try:
            item_id = int(args[0])
            attribute = args[1]
            new_val = args[2:]
            item_obj = self.client.items.item_list[item_id]
            original_val = getattr(item_obj, attribute)
            if isinstance(original_val, str):
                new_val = " ".join(new_val)
            elif isinstance(original_val, bool):
                new_val = " ".join(new_val).lower() == "true"
            elif isinstance(original_val, int):
                new_val = int("".join(new_val))
            elif isinstance(original_val, (list, dict, tuple)):
                message.channel.send("You cannot directly change this.")
                return 1
        except (KeyError, AttributeError, IndexError, ValueError)as e:
            await message.channel.send("Argument error. Message: " + e.__class__.__name__ + " : " + str(e))
            return 1
        try:
            setattr(item_obj, attribute, new_val)
            await message.channel.send(attribute + " of " + item_obj.display_name() + " with the old value of " + str(original_val) + " is changed by " + str(new_val))
        except (KeyError, AttributeError)as e:
            await message.channel.send("Argument error. Message: " + e.__class__.__name__ + " : " + str(e))
            return 1
        self.client.items.save_items_data()

    async def cmd_global_pause(self, message, args):
        if self.client.admin_role not in message.author.roles:
            return 2
        self.client.global_pause = True
        await self.client.send_announcement("Global Pause start.")

    async def cmd_global_pause_end(self, message, args):
        if self.client.admin_role not in message.author.roles:
            return 2
        self.client.global_pause = False
        await self.client.send_announcement("Global Pause ended.")

    async def cmd_ignore_this(self, message, args):
        if self.client.admin_role not in message.author.roles:
            return 2
        if message.channel.id in self.client.ignore.ignore_list:
            self.client.ignore.ignore_list.remove(message.channel.id)
            await message.channel.send("This channel is now not ignored and will be responded for commands.")
        else:
            self.client.ignore.ignore_list.append(message.channel.id)
            await message.channel.send("This channel is ignored by the bot. It will not read commands.")
        self.client.ignore.save_ignore_data()

    async def cmd_gm_status_loc(self, message, args):
        if self.client.admin_role not in message.author.roles:
            return 2
        try:
            location_name = args[0]
            location_obj = discord.utils.find(
                lambda m: self.client.rooms.room_list[m].name.lower().startswith(location_name.lower()),
                self.client.rooms.room_list)
            location_obj = self.client.rooms.room_list[location_obj]
        except (KeyError, AttributeError, IndexError, ValueError)as e:
            await message.channel.send("Argument error. Message: " + e.__class__.__name__ + " : " + str(e))
            return 1
        dict = location_obj.__dict__
        msg = location_obj.display_name() + " attributes: "
        for item in dict:
            msg += "\n" + item + " :" + str(dict[item])
        await message.channel.send(msg)

    async def cmd_gm_status_player(self, message, args):
        if self.client.admin_role not in message.author.roles:
            return 2
        try:
            player_name = args[0]
            player_obj = discord.utils.find(
                lambda m: self.client.players.player_list[m].name.lower().startswith(player_name.lower()),
                self.client.players.player_list)
            player_obj = self.client.players.player_list[player_obj]
        except (KeyError, AttributeError, IndexError, ValueError)as e:
            await message.channel.send("Argument error. Message: " + e.__class__.__name__ + " : " + str(e))
            return 1
        dict = player_obj.__dict__
        msg = player_obj.display_name() + " attributes: "
        for item in dict:
            msg += "\n" + item + " :" + str(dict[item])
        await message.channel.send(msg)

    async def cmd_gm_status_item(self, message, args):
        if self.client.admin_role not in message.author.roles:
            return 2
        try:
            item_id = int(args[0])
            item_obj = self.client.items.item_list[item_id]
        except (KeyError, AttributeError, IndexError, ValueError)as e:
            await message.channel.send("Argument error. Message: " + e.__class__.__name__ + " : " + str(e))
            return 1
        dict = item_obj.__dict__
        msg = item_obj.display_name() + " attributes: "
        for item in dict:
            msg += "\n" + item + " :" + str(dict[item])
        await message.channel.send(msg)

    async def cmd_listall_rooms(self, message, args):
        if self.client.admin_role not in message.author.roles:
            return 2
        msg = "All rooms: \n"
        for rooms in list(set(self.client.rooms.room_list.values())):
            channel = self.client.main_guild.get_channel(rooms.channel_id)
            msg += "Room " + rooms.display_name() + ": " + channel.mention + "\n"
            if len(msg) >= 1800:
                await message.channel.send(msg)
                msg = ""
        await message.channel.send(msg)

    async def cmd_listall_players(self, message, args):
        if self.client.admin_role not in message.author.roles:
            return 2
        msg = "All players: \n"
        for player in self.client.players.player_list.values():
            member = self.client.main_guild.get_member(player.id)
            msg += "Member " + player.display_name() + ": " + member.mention + "\n"
        await message.channel.send(msg)

    async def cmd_listall_items(self, message, args):
        if self.client.admin_role not in message.author.roles:
            return 2
        if args:
            startitems = int(args[0])
        else:
            startitems = 0
        msg = "All rooms: \n"
        for items in self.client.items.item_list.values():
            location = self.client.get_item_location(items.location)
            if items.id < startitems:
                continue
            if not location:
                location = " The void"
            else:
                location = location.display_name()
            msg += "Room " + items.display_name() + ", ID: " + str(items.id) + " Location: " + location + "\n"
            if len(msg) >= 1800:
                await message.channel.send(msg)
                msg = ""
        await message.channel.send(msg)

    async def cmd_get_yamls(self, message, args):
        if not self.client.check_player_role(message.author.id, self.client.admin_role):
            return 2
        get = True
        if args:
            get = False
        self.client.rooms.save_rooms_data()
        self.client.items.save_items_data()
        self.client.players.save_player_data()
        await message.channel.send("Sending settings file...")
        await message.channel.send(file=discord.File(common.YAML_SETTINGS_FILENAME))
        await message.channel.send("Sending game data files...")
        await message.channel.send(file=discord.File(common.YAML_CHANNELS_FILENAME))
        await message.channel.send(file=discord.File(common.YAML_PLAYER_FILENAME))
        await message.channel.send(file=discord.File(common.YAML_ITEM_FILENAME))
        if get:
            await message.channel.send("Sending bot data files (if any)...")
            for filename in os.listdir(common.YAML_BOT_DIR):
                await message.channel.send(file=discord.File(common.YAML_BOT_DIR + filename))
        await message.channel.send("Sending finished!")

    async def cmd_follow(self, message, args):
        if not self.client.check_player_role(message.author.id, self.client.player_role):
            return 2
        try:
            target_name = " ".join(args)
            target_obj = discord.utils.find(lambda m: self.client.players.player_list[m].name.lower().startswith(target_name.lower()), self.client.players.player_list)
            target_obj = self.client.players.player_list[target_obj]
            player_obj = self.client.players.player_list[message.author.id]
        except (IndexError, KeyError) as e:
            await message.channel.send("Argument error. Message: " + e.__class__.__name__ + " : " + str(e))
            return 1
        if target_obj.id == player_obj.id:
            await self.client.return_message(player_obj, message, "You can't follow yourself.")
            return 1
        check_hold = self.client.players.check_holding(player_obj.id)
        if check_hold:
            await self.client.return_message(player_obj, message, "You cannot follow someone! You are grabbed by" + check_hold + "!")
            return 1
        if self.client.same_area_player_check(player_obj, target_obj):
            await self.client.return_message(player_obj, message, "You have started following " + target_obj.name + ".")
            player_obj.following = target_obj.id
            self.client.players.save_player_data()
            return 1
        else:
            await self.client.return_message(player_obj, message, "You cannot see that person!")
            return 1

    async def cmd_unfollow(self, message, args):
        if not self.client.check_player_role(message.author.id, self.client.player_role):
            return 2
        try:
            player_obj = self.client.players.player_list[message.author.id]
            if player_obj.following == 0:
                await self.client.return_message(player_obj, message,
                                                 "You are not following anyone.")
                return 1
            target_obj = self.client.players.player_list[player_obj.following]
        except (IndexError, KeyError) as e:
            await message.channel.send("Argument error. Message: " + e.__class__.__name__ + " : " + str(e))
            return 1
        await self.client.return_message(player_obj, message,
                                         "You have stopped following " + target_obj.name + ".")
        player_obj.following = 0
        self.client.players.save_player_data()

    async def cmd_knock(self, message, args):
        if not self.client.check_player_role(message.author.id, self.client.player_role):
            return 2
        try:
            move_name = "_".join(args)
            knock_obj = discord.utils.find(lambda m: self.client.rooms.room_list[m].name.lower().
                                              startswith(move_name.lower()), self.client.rooms.room_list)
            knock_obj = self.client.rooms.room_list[knock_obj]
            player_obj = self.client.players.player_list[message.author.id]
            location_obj = self.client.rooms.room_list[player_obj.location]
        except (KeyError, ValueError):
            await message.channel.send("Invalid location.")
            return 2
        knock_channel = self.client.rooms.get_room_obj(id=knock_obj.channel_id)
        location_channel = self.client.rooms.get_room_obj(id=location_obj.channel_id)
        await knock_channel.send("Knock knock! A knock is coming from " + location_obj.display_name() + "!")
        await location_channel.send(player_obj.display_name() + " knocks on " + knock_obj.display_name() + "!")

    async def cmd_play_music(self, message, args):
        if self.client.admin_role not in message.author.roles:
            return 2
        try:
            bot_id = int(args[0])
            filename = " ".join(args[1:])
            bot_obj = self.client.musicbots[bot_id]
        except (KeyError, ValueError):
            await message.channel.send("Invalid arguments.")
            return 2
        await bot_obj.play_music(filename)

    async def cmd_music_setup(self, message, args):
        if self.client.admin_role not in message.author.roles:
            return 2
        for bot in self.client.musicbots.values():
            await bot.join_voice()

    async def cmd_inventory(self, message, args):
        if not self.client.check_player_role(message.author.id, self.client.player_role):
            return 2
        try:
            player_obj = self.client.players.player_list[message.author.id]
            item_list = self.client.return_inventory(player_obj.id, 0)
        except (KeyError, AttributeError, IndexError, ValueError)as e:
            await message.channel.send("Argument error. Message: " + e.__class__.__name__ + " : " + str(e))
            return 2
        if item_list is None:
            await self.client.return_message(player_obj, message, "You have no items!")
            return 1
        msg = "You have the following items: \n"
        for item in item_list:
             msg += item.display_name() + "\n"
        await self.client.return_message(player_obj, message, msg)

    async def cmd_truthbullets(self, message, args):
        if not self.client.check_player_role(message.author.id, self.client.player_role):
            return 2
        try:
            player_obj = self.client.players.player_list[message.author.id]
            item_list = self.client.return_inventory(player_obj.id, 1)
        except (KeyError, AttributeError, IndexError, ValueError)as e:
            await message.channel.send("Argument error. Message: " + e.__class__.__name__ + " : " + str(e))
            return 2
        if item_list is None:
            await self.client.return_message(player_obj, message, "You have no items!")
            return 1
        msg = "You have the following Truth Bullets: \n"
        for item in item_list:
             msg += item.display_name() + "\n"
        await self.client.return_message(player_obj, message, msg)

    async def cmd_shoot(self, message, args):
        if self.client.check_player_role(message.author.id, self.client.admin_role) or self.client.check_player_role(message.author.id, self.client.spec_role):
            return 2
        try:
            item_name = args[0]
            player_obj = self.client.players.player_list[message.author.id]
            location_obj = self.client.rooms.room_list[player_obj.location]
            channel_obj = self.client.main_guild.get_channel(location_obj.channel_id)
        except (IndexError, KeyError) as e:
            await message.channel.send("Argument error. Message: " + e.__class__.__name__ + " : " + str(e))
            return 1
        item_list = self.client.get_all_inventory(player_obj.id)
        item_list = self.client.items.convert_list(item_list)
        item_obj = self.client.items.find_item_in_list(item_name, item_list)
        if not item_obj.truth_bullet:
            await self.client.return_message(player_obj, message, "That cannot be shot.")
            return 1
        if item_obj:
            await channel_obj.send(player_obj.display_name() + " shoots " + item_obj.display_name() + ": \n" + item_obj.desc)
            return 1
        else:
            await self.client.return_message(player_obj, message, "Cannot find said object.")

    async def cmd_delitem(self, message, args):
        if self.client.check_player_role(message.author.id, self.client.player_role) or self.client.check_player_role(message.author.id, self.client.spec_role):
            return 2
        item_id = args[0]
        try:
            item_obj = self.client.items.item_list[int(item_id)]
        except (KeyError, ValueError):
            await message.channel.send("Argument error. Message: " + e.__class__.__name__ + " : " + str(e))
            return 1
        if item_obj:
            del self.client.items.item_list[item_obj.id]
            self.client.delete_inventory(item_obj.id)
            try:
                test = self.client.items.item_list[int(item_id)]
                await message.channel.send("Error in deletion.")
            except (IndexError, KeyError):
                await message.channel.send("Item " + item_obj.display_name() +" deleted.")
                self.client.items.fix_numbers()
            self.client.items.save_items_data()
            self.client.rooms.save_rooms_data()
            self.client.players.save_player_data()
            return 0
        await message.channel.send("Item does not exist.")
        return 1

    async def cmd_searchitem(self, message, args):
        if self.client.check_player_role(message.author.id, self.client.player_role) or self.client.check_player_role(
                message.author.id, self.client.spec_role):
            return 2
        item_name = " ".join(args)
        try:
            item_list = list(filter(lambda m: item_name.lower() in m.display_name().lower(), self.client.items.item_list.values()))
        except (KeyError, ValueError) as e:
            await message.channel.send("Argument error. Message: " + e.__class__.__name__ + " : " + str(e))
            return 1
        if item_list:
            msg = "Items Found: \n"
            for item in item_list:
                msg += item.display_name() + " ID: " + str(item.id) + "\n"
            await message.channel.send(msg)
        else:
            await message.channel.send("Item not found.")

    async def cmd_remove_truthbullet(self, message, args):
        if self.client.check_player_role(message.author.id, self.client.player_role) or self.client.check_player_role(
                message.author.id, self.client.spec_role):
            return 2
        new_list = self.client.items.return_truthbullets()
        for item in new_list:
            del self.client.items.item_list[item.id]
            self.client.delete_inventory(item.id)
        self.client.items.fix_numbers()
        await message.channel.send("Truth bullets deleted.")

    async def cmd_list_inventories(self, message, args):
        if self.client.check_player_role(message.author.id, self.client.player_role) or self.client.check_player_role(
                message.author.id, self.client.spec_role):
            return 2
        txtmsg = "List of player inventories: \n"
        for player in self.client.players.player_list.values():
            txtmsg += player.name + ": \n"
            for item in player.inventory:
                item_obj = self.client.items.item_list[item]
                txtmsg += "  " + item_obj.display_name() + "\n"
        await message.channel.send(txtmsg)

    async def cmd_map(self, message, args):
        if self.client.check_player_role(message.author.id, self.client.admin_role) or self.client.check_player_role(message.author.id, self.client.spec_role):
            return 2
        try:
            player_obj = self.client.players.player_list[message.author.id]
            location_obj = self.client.rooms.room_list[player_obj.location]
        except (IndexError, KeyError) as e:
            await message.channel.send("Argument error. Message: " + e.__class__.__name__ + " : " + str(e))
            return 1
        all_list = self.client.searcher.list_all_movable_areas(location_obj.name)
        map = {}
        msg = ""
        for node in all_list:
            room_obj = self.client.rooms.room_list[node.name]
            channel_obj = self.client.main_guild.get_channel(room_obj.channel_id)
            try:
                map[channel_obj.category.name]
            except KeyError:
                map[channel_obj.category.name] = []
            map[channel_obj.category.name].append(room_obj)
        for k, v in sorted(map.items()):
            msg += "**" + k + "** \n"
            for i in v:
                msg += i.display_name() + " ( " + i.shortcut + " )" + "\n"
        await message.channel.send(msg)

