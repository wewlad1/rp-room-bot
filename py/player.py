# Copyright 2019 Mystery Coder X <anonbaka9@hotmail.com>
#
# Licensed under the Microsoft Public License (Ms-PL).

import py.common as common
import discord
import yaml
import os


class Player:

    def __init__(self, member):
        self.name = member.display_name
        self.id = member.id
        self.notepad = ""
        self.notepad_setting = 0
        self.desc = ""
        self.location = None
        self.hidden = False
        self.movement = True
        self.holding = 0
        self.inventory = []
        self.hidden_inventory = []
        self.bot = False
        self.bot_id = 0
        self.following = 0

    def get_object(self, client):
        client.players.get_player_obj(id=self.id)

    def display_name(self):
        return self.name.replace("_", " ").capitalize()
class PlayerHandler:

    def __init__(self, client):
        self.client = client
        self.player_list = {}
        self.load_player_data()

    async def initialize_players(self):
        await self.create_players()
        self.save_player_data()

    def check_object(self, player):
        if not isinstance(player, Player):
            return player
        new_player = Player(self.get_player_obj(id=player.id))
        for attribute in new_player.__dict__:
            if not hasattr(player, attribute):
                return self.update_object(player, new_player)

    def update_object(self, player, new_player):
        for attribute in new_player.__dict__:
            try:
                old_attr = getattr(player, attribute)
                setattr(new_player, attribute, old_attr)
            except AttributeError:
                continue
        return new_player

    def find_player(self, playername):
        player = discord.utils.find(lambda m: self.player_list[m].name.lower().
                                              startswith(playername.lower()), self.player_list)
        if not player:
            player = discord.utils.find(lambda m: self.player_list[m].display_name().lower().
                                              startswith(playername.lower()), self.player_list)
        if not player:
            return None
        else:
            return player

    async def create_players(self):
        for member in self.client.main_guild.members:
            has_role = discord.utils.find(lambda m: m.name == self.client.settings.player_role, member.roles)
            try:
                has_obj = self.player_list[member.id]
            except KeyError:
                has_obj = None
            if has_role and not has_obj:
                new_player = Player(member)
                if not member.bot:
                    await self.create_notepad_channel(self.client, new_player, member)
                self.player_list[new_player.id] = new_player
        self.save_player_data()

    async def create_notepad_channel(self, client, player, member):
        perms_dict = {}
        perms_notallowed = discord.PermissionOverwrite()
        perms_allowed = discord.PermissionOverwrite()
        perms_notallowed.read_messages = False
        perms_notallowed.send_messages = False
        perms_allowed.read_messages = True
        perms_allowed.send_messages = True
        perms_dict[client.player_role] = perms_notallowed
        perms_dict[client.spec_role] = perms_notallowed
        perms_dict[client.everyone_role] = perms_notallowed
        perms_dict[member] = perms_allowed
        obj = await client.main_guild.create_text_channel(player.name + "_notes", overwrites=perms_dict)
        player.notepad = obj.id

    def get_player_obj(self, **kwargs):
        name_r = kwargs.get('name', None)
        id_r = kwargs.get('id', None)
        if id_r:
            return self.client.main_guild.get_member(id_r)
        if name_r:
            return discord.utils.find(lambda m: m.display_name.lower() == name_r.lower(), self.client.main_guild.members)

    def check_holding(self, player_id):
        for player in self.player_list.values():
            if player_id == player.holding:
                return player.name
        return ""

    def get_player_followers(self, player_id):
        follower_list = []
        for player in self.player_list.values():
            if player_id == player.following:
                follower_list.append(player)
        return follower_list


    def load_player_data(self):
        try:
            with open(common.YAML_PLAYER_FILENAME, 'r') as stream:
                try:
                    self.player_list = yaml.unsafe_load(stream)
                    if not self.player_list:
                        self.player_list = {}
                except yaml.YAMLError as exc:
                    print(exc)
        except FileNotFoundError:
            self.save_player_data()

    def save_player_data(self):
        try:
            os.mkdir(common.YAML_GAMEDATA_FOLDER)
        except FileExistsError:
            pass
        with open(common.YAML_PLAYER_FILENAME, 'w') as outfile:
            yaml.dump(self.player_list, outfile, default_flow_style=False)
