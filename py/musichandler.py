# Copyright 2019 Mystery Coder X <anonbaka9@hotmail.com>
#
# Licensed under the Microsoft Public License (Ms-PL).

import discord
import asyncio
import py.common as common


class MusicBot(discord.client.Client):

    def __init__(self, botsettings, mainclient):
        discord.client.Client.__init__(self)
        self.settings = botsettings
        self.music_path = common.YAML_MUSIC_DIR
        self.main_client = mainclient
        self.guild = None
        self.member = None
        self.voice = None
        self.voice_channel = None
        self.voice_client = None
        self.playing = None
        self.id = None
        self.main_client.loop.create_task(self.start(self.settings.token))

    async def on_ready(self):
        await self.wait_until_ready()
        await self.initialize_items()

    async def initialize_items(self):
        self.guild = self.get_guild(int(self.settings.guild))
        if self.guild is None:
            raise AttributeError("Invalid server ID")
        self.voice = int(self.settings.voice)
        self.member = self.guild.get_channel(int(self.settings.player))
        self.id = int(self.settings.id)
        self.voice_channel = self.guild.get_channel(self.voice)
        if self.voice_channel is None:
            raise AttributeError("Invalid voice channel.")
        voice_recon = discord.utils.find(lambda m: m.id == self.user.id, self.voice_channel.members)
        if voice_recon:
            await voice_recon.move_to(None)

    async def join_voice(self):
        self.voice_client = await self.voice_channel.connect(reconnect=False)

    async def play_music(self, filename):
        if self.voice_client.is_playing():
            self.voice_client.stop()
        self.voice_client.play(discord.FFmpegPCMAudio(source=self.music_path + filename),
                               after=lambda m: self.play_again(filename))

    def play_again(self, filename):
        if self.voice_client.is_playing():
            return
        asyncio.run_coroutine_threadsafe(self.play_music(filename), self.loop)

    def pause_music(self):
        if self.voice_client.is_playing():
            self.voice_client.pause()

    def resume_music(self):
        if self.voice_client.is_paused():
            self.voice_client.pause()
            return True
        else:
            return False

    def stop_music(self):
        if self.voice_client.is_playing():
            self.voice_client.stop()
            return True
        else:
            return False

