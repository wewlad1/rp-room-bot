# Copyright 2019 Mystery Coder X <anonbaka9@hotmail.com>
#
# Licensed under the Microsoft Public License (Ms-PL).


class Searcher:

    def __init__(self, client):
        self.client = client

    def search_path(self, start, goal):
        node_list = []
        all_node = []
        start_node = self.new_node(start)
        start_node.discovered = True
        node_list.append(start_node)
        while node_list:
            v = node_list.pop(0)
            if v.name == goal:
                path_list = []
                path_list.append(v.name)
                x = False
                node_finder = v
                while not x:
                    search_node = self.find_node(all_node, node_finder.parent)
                    if search_node[0]:
                        node = search_node[1]
                        path_list.append(node.name)
                        node_finder = node
                        x = node_finder.name == start
                    else:
                        if node_finder.parent == start:
                            path_list.append(node_finder.parent)
                        break
                path_list.reverse()
                return path_list
            for member in self.connection_list(v.name):
                search_node = self.find_node(all_node, member)
                if search_node[0]:
                    if not search_node[1].discovered:
                        search_node[1].discovered = True
                        search_node[1].parent = v.name
                        all_node.append(search_node[1])
                        node_list.append(search_node[1])
                else:
                    new_node = self.new_node(member)
                    new_node.discovered = True
                    new_node.parent = v.name
                    all_node.append(new_node)
                    node_list.append(new_node)
        return []

    def list_all_movable_areas(self, location):
        node_list = []
        all_node = []
        start_node = self.new_node(location)
        start_node.discovered = True
        node_list.append(start_node)
        while node_list:
            v = node_list.pop(0)
            for member in self.connection_list(v.name):
                if self.maplock(member):
                    continue
                search_node = self.find_node(all_node, member)
                if search_node[0]:
                    if not search_node[1].discovered:
                        search_node[1].discovered = True
                        search_node[1].parent = v.name
                        all_node.append(search_node[1])
                        node_list.append(search_node[1])
                else:
                    new_node = self.new_node(member)
                    new_node.discovered = True
                    new_node.parent = v.name
                    all_node.append(new_node)
                    node_list.append(new_node)
        return all_node

    def new_node(self, name):
        new_node = Node()
        new_node.name = name
        return new_node

    def find_node(self, node_list, find):
        for node in node_list:
            if find == node.name:
                return True, node
        return False, None

    def connection_list(self, name):
        room_obj = self.client.rooms.room_list[name]
        return room_obj.connection


    def maplock(self, name):
        room_obj = self.client.rooms.room_list[name]
        return room_obj.maplock

class Node:

    def __init__(self):
        self.name = ""
        self.parent = ""
        self.discovered = False

