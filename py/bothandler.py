# Copyright 2019 Mystery Coder X <anonbaka9@hotmail.com>
#
# Licensed under the Microsoft Public License (Ms-PL).

import discord
import asyncio
import py.common as common

class PlayerBot(discord.client.Client):

    def __init__(self, botsettings, mainclient):
        discord.client.Client.__init__(self)
        self.settings = botsettings
        self.main_client = mainclient
        self.guild = None
        self.channel = None
        self.location_channel = None
        self.ooc_channel = None
        self.bot_channel = None
        self.id = None
        self.main_client.loop.create_task(self.start(self.settings.token))

    async def on_ready(self):
        await self.wait_until_ready()
        self.initialize_items()

    def initialize_items(self):
        self.guild = self.get_guild(int(self.settings.guild))
        if self.guild is None:
            raise AttributeError("Invalid server ID")
        self.channel = self.guild.get_channel(int(self.settings.channel))
        self.bot_channel = self.guild.get_channel(int(self.settings.command))
        self.ooc_channel = self.guild.get_channel(int(self.settings.ooc))
        self.id = int(self.settings.id)

    def update_location_channel(self):
        player = discord.utils.find(lambda m: m.bot_id == self.id, self.main_client.players.player_list.values())
        if player.location:
            self.location_channel = self.guild.get_channel(player.location)

    async def on_message(self, message):
        if message.author == self.user:
            return
        if message.channel.id == self.ooc_channel.id:
            return
        if message.channel.id == self.bot_channel.id:
            return
        self.update_location_channel()
        if message.content.startswith(common.YAML_COMMAND_CHAR) and not message.channel.id == self.location_channel.id:
            await self.bot_channel.send(message.content)
            return
        if message.channel.id == self.location_channel.id:
            name = message.author.nick
            if name is None:
                name = message.author.name
            await self.channel.send(name + ": " + message.content)
        elif message.channel.id == self.channel.id:
            if message.content.startswith('ooc:'):
                await self.ooc_channel.send(message.content[4:])
            else:
                await self.location_channel.send(message.content)
