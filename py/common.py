# Copyright 2019 Mystery Coder X <anonbaka9@hotmail.com>
#
# Licensed under the Microsoft Public License (Ms-PL).

import discord


YAML_SETTINGS_FOLDER = "settings/"
YAML_GAMEDATA_FOLDER = "settings/gamedata"
YAML_SETTINGS_FILENAME = "settings/settings.yaml"
YAML_CHANNELS_FILENAME = "settings/gamedata/rooms.yaml"
YAML_PLAYER_FILENAME = "settings/gamedata/player.yaml"
YAML_ITEM_FILENAME = "settings/gamedata/items.yaml"
YAML_IGNORE_FILENAME = "settings/gamedata/ignorelist.yaml"
YAML_BOT_DIR = "settings/botdata/"
YAML_MUSICBOT_DIR = "settings/musicbot/"
YAML_MUSIC_DIR = "music/"

YAML_TOKEN_NAME = 'token'
YAML_VOICECHAT_NAME = 'voicechat'
YAML_PLAYER_NAME = 'player'
YAML_GUILD_NAME = 'server'
YAML_CHANNEL_NAME = 'channel'
YAML_ANNOUNCEMENT_CHANNEL = 'announcement_channel'

YAML_PLAYER_ROLE = 'player_role'
YAML_SPECTATOR_ROLE = 'spec_role'
YAML_ADMIN_ROLE = 'admin_role'
YAML_COMMAND_CHAR = '!'

YAML_ID_NAME = 'id'
YAML_BOTCOMMAND_NAME = 'botcommand'
YAML_OOC_NAME = 'ooc'
YAML_CHANNEL_CATEGORY = 'category'
YAML_CHANNEL_DESCRIPTION = 'description'
YAML_STARTS_LOCKED = 'starts_locked'
YAML_CHANNEL_ID = 'channel_id'

DISCORD_ADMIN_COLOUR = discord.colour.Color.from_rgb(220, 20, 60)
DISCORD_PLAYER_COLOUR = discord.colour.Color.from_rgb(135, 206, 235)
DISCORD_SPECTATOR_COLOUR = discord.colour.Color.from_rgb(255, 0, 255)

DICT_DEFAULT_SETTINGSYAML = {
        YAML_TOKEN_NAME:     '[put Discord Bot Token here]',
        YAML_GUILD_NAME:     '[put Discord Server ID here]',
        YAML_PLAYER_ROLE:    '[put name of player role here]',
        YAML_SPECTATOR_ROLE: '[put name of spectator role here]',
        YAML_ADMIN_ROLE:     '[put name of admin role here]',
    }

LIST_DEFAULT_ROOMSYAML = ["void"]

DICT_DEFAULT_CHANNELDICT = {
    YAML_CHANNEL_NAME: 'void',
    YAML_CHANNEL_CATEGORY: 'Information',
    YAML_CHANNEL_DESCRIPTION: 'description',
    YAML_STARTS_LOCKED: False,
    YAML_CHANNEL_ID: 0,
}

ERROR_STRING_1 = "The server settings has not been initialized. Please check" + YAML_SETTINGS_FILENAME
ERROR_STRING_2 = "The settings is invalid or has wrong syntax. Delete settings.yaml and run the program again to make" \
                 " another settings.yaml."
ERROR_STRING_3 = "The channel settings has not been initialized. Please check " + YAML_CHANNELS_FILENAME

NORMAL_WELCOME_MESSAGE = "Program Active."