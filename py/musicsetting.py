# Copyright 2019 Mystery Coder X <anonbaka9@hotmail.com>
#
# Licensed under the Microsoft Public License (Ms-PL).

import yaml
import sys
import os
import py.common as common


class MusicSettings:

    def __init__(self, path):
        self.path = path
        self.data = self.load_settings_yaml()
        try:
            self.id = self.data[common.YAML_ID_NAME]
            self.player = self.data[common.YAML_PLAYER_NAME]
            self.token = self.data[common.YAML_TOKEN_NAME]
            self.guild = self.data[common.YAML_GUILD_NAME]
            self.voice = self.data[common.YAML_VOICECHAT_NAME]
        except KeyError:
            sys.exit(common.ERROR_STRING_2)

    def load_settings_yaml(self):
        try:
            with open(self.path, 'r') as stream:
                try:
                    return yaml.safe_load(stream)
                except yaml.YAMLError as exc:
                    print(exc)
        except FileNotFoundError:
            sys.exit(common.ERROR_STRING_1)