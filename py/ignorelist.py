# Copyright 2019 Mystery Coder X <anonbaka9@hotmail.com>
#
# Licensed under the Microsoft Public License (Ms-PL).

import yaml
import sys
import os
import py.common as common


class IgnoreList:

    def __init__(self):
        self.ignore_list = self.load_ignore_yaml()

    def load_ignore_yaml(self):
        try:
            with open(common.YAML_IGNORE_FILENAME, 'r') as stream:
                try:
                    return yaml.safe_load(stream)
                except yaml.YAMLError as exc:
                    print(exc)
        except FileNotFoundError:
            try:
                os.mkdir(common.YAML_SETTINGS_FOLDER)
            except FileExistsError:
                pass
            with open(common.YAML_IGNORE_FILENAME, 'w') as outfile:
                yaml.dump([], outfile, default_flow_style=False)
            sys.exit(common.ERROR_STRING_1)

    def save_ignore_data(self):
        try:
            os.mkdir(common.YAML_GAMEDATA_FOLDER)
        except FileExistsError:
            pass
        with open(common.YAML_IGNORE_FILENAME, 'w') as outfile:
            yaml.dump(self.ignore_list, outfile, default_flow_style=False)
