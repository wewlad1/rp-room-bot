# Copyright 2019 Mystery Coder X <anonbaka9@hotmail.com>
#
# Licensed under the Microsoft Public License (Ms-PL).

import yaml
import sys
import os
import py.common as common


class Settings:

    def __init__(self):
        self.data = self.load_settings_yaml()
        try:
            self.token = self.data[common.YAML_TOKEN_NAME]
            self.admin_role = self.data[common.YAML_ADMIN_ROLE]
            self.player_role = self.data[common.YAML_PLAYER_ROLE]
            self.spec_role = self.data[common.YAML_SPECTATOR_ROLE]
            self.guild = self.data[common.YAML_GUILD_NAME]
            self.announcement_channel = self.data[common.YAML_ANNOUNCEMENT_CHANNEL]
        except KeyError:
            sys.exit(common.ERROR_STRING_2)

    @staticmethod
    def load_settings_yaml():
        try:
            with open(common.YAML_SETTINGS_FILENAME, 'r') as stream:
                try:
                    return yaml.safe_load(stream)
                except yaml.YAMLError as exc:
                    print(exc)
        except FileNotFoundError:
            try:
                os.mkdir(common.YAML_SETTINGS_FOLDER)
            except FileExistsError:
                pass
            with open(common.YAML_SETTINGS_FILENAME, 'w') as outfile:
                yaml.dump(common.DICT_DEFAULT_SETTINGSYAML, outfile, default_flow_style=False)
            sys.exit(common.ERROR_STRING_1)
