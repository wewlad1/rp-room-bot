# Copyright 2019 Mystery Coder X <anonbaka9@hotmail.com>
#
# Licensed under the Microsoft Public License (Ms-PL).

import py.common
import py.settings as settings
import py.client as client


if __name__ == '__main__':
    ProgramSettings = settings.Settings()
    RoomRPBotProcess = client.Client(ProgramSettings)


# Loads program settings from /settings
